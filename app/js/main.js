// чекбокс для отчества
let checkbox = document.querySelector('#checkbox');
checkbox.onchange = () => ((surname.disabled = checkbox.checked) & (surname.value = null));

// щткрытие попапа
let openPopup = document.querySelector('.cart__add-product'); // Кнопки для показа окна
let closePopup = document.querySelector('.popup__close');
let closePopupButton = document.querySelector('.popup__submit');
let popup = document.querySelector('.popup');

openPopup.addEventListener('click',(e) => {
  e.preventDefault();
  popup.classList.remove('hidden')
});
closePopupButton.addEventListener('click',(e) => {
  e.preventDefault();
  popup.classList.add('hidden')
  // тут код добавления товара
});
closePopup.addEventListener('click',(e) => {
  e.preventDefault();
  popup.classList.add('hidden')
});

// ----- переключалка блоков формы -------
let currentTab = 0; // Текущий блок = первый юлок
showTab(currentTab); // Отображение текущего блока

//фиксируем нужные кнопки в юлоках
function showTab(n) {
  let x = document.querySelectorAll('.form__fields');
  x[n].classList.remove('hidden');
  if (n == 0) {
    document.getElementById('prevBtn').classList.add('hidden');
  } else {
    document.getElementById('prevBtn').classList.remove('hidden');
  }
  if (n == (x.length - 1)) {
    document.getElementById('nextBtn').setAttribute('onclick', "document.location='pincode.html'");
    document.querySelector('.agree').classList.remove('hidden');
    document.querySelector('.form__footer').classList.remove('form__footer--reverse');
  } else {
    document.getElementById('nextBtn').setAttribute('onclick', 'nextPrev(1)');
    document.querySelector('.agree').classList.add('hidden');
    document.querySelector('.form__footer').classList.add('form__footer--reverse');
  }
}

// Отображаем нужный блок
function nextPrev(n) {
  let x = document.querySelectorAll('.form__fields');
  x[currentTab].classList.add('hidden');
  currentTab = currentTab + n;
  showTab(currentTab);
}
